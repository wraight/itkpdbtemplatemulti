### standard
import streamlit as st
import streamlit.components.v1 as components
import extra_streamlit_components as stx
from core.Page import Page
### custom
import datetime
import os
import sys
import json
import subprocess
import ast
from annotated_text import annotated_text, annotation
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra

#####################
### Top page
#####################

### format datetime
def DateFormat(dt):
    return str("{0:02}-{1:02}-{2:04}".format(dt.day,dt.month,dt.year))+" at "+str("{0:02}:{1:02}".format(dt.hour,dt.minute))


def GetCodes(args):
    code1,code2=None,None
    for a in args:
        if "ac1" in a: code1=a[4::].strip('"').strip("'")
        if "ac2" in a: code2=a[4::].strip('"').strip("'")
    return code1,code2


#####################
### main part
#####################

class Pagea(Page):
    def __init__(self):
        super().__init__("Cookies", ":cookie: Test Cookie manipulation", ['nothing to report'])

    def main(self):
        super().main()

        nowTime = datetime.datetime.now()
        st.write("""### :calendar: ("""+DateFormat(nowTime)+""")""")



        # ### get cookie manager
        # cookie_manager = stx.CookieManager()

        # st.subheader("All Cookies:")
        # cookies = cookie_manager.get_all()
        # st.write(cookies)

        # c1, c2, c3 = st.columns(3)

        # with c1:
        #     st.subheader("Get Cookie:")
        #     cookie = st.text_input("Cookie", key="0")
        #     clicked = st.button("Get")
        #     if clicked:
        #         value = cookie_manager.get(cookie=cookie)
        #         st.write(value)
        # with c2:
        #     st.subheader("Set Cookie:")
        #     cookie = st.text_input("Cookie", key="1")
        #     val = st.text_input("Value")
        #     if st.button("Add"):
        #         cookie_manager.set(cookie, val) # Expires in a day by default
        # with c3:
        #     st.subheader("Delete Cookie:")
        #     cookie = st.text_input("Cookie", key="2")
        #     if st.button("Delete"):
        #         cookie_manager.delete(cookie)
        #         cookie_manager = get_manager()


        if st.checkbox("Check cookies?"):
            st.subheader("All Cookies:")
            cookie_manager = stx.CookieManager()
            cookies = cookie_manager.get_all()
            st.write(cookies)
            st.write("- please refresh page after update")

            if st.checkbox("Update/add Cookie?"):
                cookie = st.text_input("Cookie", key="1")
                val = st.text_input("Value")
                if st.button("Add"):
                    cookie_manager.set(cookie, val) # Expires in a day by default
                    st.write(f"updated {cookie}: {cookie_manager.get(cookie=cookie)}")
        
            if st.checkbox("Delete Cookie?"):
                cookie = st.text_input("Cookie", key="2")
                if st.button("Delete"):
                    cookie_manager.delete(cookie)
            
            st.write("__NB__ Changes will take effect after refresh")