### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import pandas as pd
import datetime
import os
import sys
import json
import subprocess
import ast
from annotated_text import annotated_text, annotation
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra

### analyses

#####################
### useful functions
#####################

infoList=[" * where to find reports and more"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("reports", "📶 Directions to Reports and *more*", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        st.write("## Looking for a report or how to make one?")

        st.write("Please navigate to the links below:")

        pageList=[
            # report collections
            {'name':"Reporting Hub", 'description':"Collection of ITk PDB reports", 'link': "https://itk-pdb-reporting-hub.docs.cern.ch"},
            {'name':"Data Integrity Reports", 'description':"Collection of ITk PDB data integrity reports", 'link': "https://itk-reporting-repository.docs.cern.ch"},
            # development
            {'name':"PDB Structure", 'description':"Structure of PDB Objects", 'link': "https://itk-pdb-structure-repository.docs.cern.ch"},
            {'name':"Reportorium", 'description':"Tools to help ITk PDB reporting development", 'link': "https://itk-pdb-reportorium.docs.cern.ch"},
            {'name':"ITk DI reports", 'description':"Framework for ITk PDB reports", 'link': "https://itk-reports.docs.cern.ch"},
            {'name':"Streamlit", 'description':"Tutorial for Streamlit webApp", 'link': "https://streamlit-for-itk.docs.cern.ch"}
        ]

        # st.dataframe(pageList)

        for pl in pageList:
            st.write(f"__{pl['name']}__ : {pl['description']}, [link]({pl['link']})")

        st.write("---")
        # st.write("### More information")
    