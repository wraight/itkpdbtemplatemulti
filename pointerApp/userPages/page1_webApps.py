### standard
import streamlit as st
import streamlit.components.v1 as components
from core.Page import Page
### custom
import datetime
import os
import sys
import json
import subprocess
import ast
from annotated_text import annotated_text, annotation
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra

### analyses

#####################
### useful functions
#####################

infoList=[" * where to find webApps"]
#####################
### main part
#####################

class Page1(Page):
    def __init__(self):
        super().__init__("webApps", "⤴ Directions to ITk webApps", infoList)

    def main(self):
        super().main()

        ### getting attribute
        pageDict=st.session_state[self.name]

        st.write("## Looking for a webApp?")

        st.write("Please navigate to your favourite app from the links below:")

        appList=[
            {'name':"pixels", "description":"collections of pixels webApps", 'link':"https://itk-pdb-webapps-pixels.web.cern.ch"},
            {'name':"strips", "description":"collection of strips webApps", 'link':"https://itk-pdb-webapps-strips.web.cern.ch"},
            {'name':"common electronics", "description":"collection of common electronics webApps", 'link':"https://itk-pdb-webapps-common-electronics.web.cern.ch"},
            {'name':"general", "description":"infrequently updated testing app", 'link':"https://itk-pdb-webapps-general.web.cern.ch"},
            {'name':"reporting", "description":"webApps for reporting", 'link':"https://itk-pdb-webapps-reporting.web.cern.ch"},
            {'name':"report generation", "description":"generate reports on demand", 'link':"https://nihad-itk-qt-itk-reporting.app.cern.ch"}
        ]
        
        # st.dataframe(appList)

        for al in appList:
            st.write(f"__{al['name'].title()}__ : {al['description']}, [link]({al['link']})")
        
        st.write("---")
        st.write("### More information")
        
        moreList=[
            {'name':"webApps", "description":"gitLab repo. for webApps", 'link':"https://gitlab.cern.ch/wraight/itk-web-apps"},
            {'name':"itk-docs", "description":"documentation on much ITk activity", 'link':"https://itk.docs.cern.ch"}
        ]
        
        for ml in moreList:
            st.write(f"__{ml['name'].title()}__ : {ml['description']}, [link]({ml['link']})")

    