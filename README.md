# ITk PDB Template

### [Streamlit](https://www.streamlit.io) (python**3**) code to upload component/test schemas for registration on production database (PDB)

**NB** Powered by [itkdb](https://pypi.org/project/itkdb/)

Check requirements file for necessary libraries

---

## WebApp Layout

**Basic Idea:**
* template for ITk webApps
* based on: [*streamlit template multi*](https://github.com/kwraight/streamlitTemplateMulti)

### Main Page (Authentication)
  * input PDB access codes (only stored in browser cache)

### Content supplied by user
  * TBA

### Debug Page:
**(Broom cupboard)**
  * Session state settings

---

## Adding content

See example page for template structure.
The procedure is as follows:

1. make a ''userPages'' sub-directory

2. add files to directory (use prefix: ''page_'')
  * add number suffix to order pages: e.g. _page1.py_ name will be first in sidebar selection.

---

## Running locally

Run webApp locally:

* get required libraries:
> python3 -m pip install -r requirements

* run streamlit:
> streamlit run mainApp.py
  * option to add credentials at end with format: ''ac1:X ac2:Y''

* open browser at ''localhost:8501''

---

## Running via Docker

Either of two files can be used to build basic templates (structural files):

build image (*usual*):

> docker build . -f dockerFiles/Dockerfile -t itk-app

The build will copy files in the _userPages_ directory into the image and use these as content for the webApp.

* run container from image (mapping ports):

> docker run -p 8501:8501 new-app

* open browser at ''localhost:8501''


build *Cern usable* image:
* this will run app as non-root user (for use with openShift)

> docker build . -f dockerFiles/DockerfileCern -t itk-app


The build will copy files in the _userPages_ directory into the image and use these as content of the webApp.

* run container from image (mapping ports):

> docker run -p 8501:8501 itk-app

* open browser at ''localhost:8501''

---

## Running with mounted volumes

This allows changes to files in mounted directory to be propagated to container immediately (*i.e.* without docker rebuilds) - useful for development!
**NB** this will overwrite any files in linked directory:

> docker run -p 8501:8501 -v $(pwd)/userPages:/code/userPages itk-app
