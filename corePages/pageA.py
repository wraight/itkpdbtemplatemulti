### standard
import streamlit as st
import streamlit.components.v1 as components
import extra_streamlit_components as stx
from core.Page import Page
### custom
import datetime
import os
import sys
import json
import subprocess
import ast
from annotated_text import annotated_text, annotation
### PDB stuff
import itkdb
import core.DBaccess as DBaccess
import core.stInfrastructure as infra

#####################
### Top page
#####################

### format datetime
def DateFormat(dt):
    return str("{0:02}-{1:02}-{2:04}".format(dt.day,dt.month,dt.year))+" at "+str("{0:02}:{1:02}".format(dt.hour,dt.minute))

### get PDB access codes
def GetCodes(args):
    code1,code2=None,None
    for a in args:
        if "ac1" in a: code1=a[4::].strip('"').strip("'")
        if "ac2" in a: code2=a[4::].strip('"').strip("'")
    return code1,code2

# @st.cache_resource
def get_manager():
    return stx.CookieManager()

#####################
### main part
#####################

class Pagea(Page):
    def __init__(self):
        super().__init__("Authenticate", ":ticket: Get Your Ticket!", ['nothing to report'])

    def main(self):
        super().main()

        nowTime = datetime.datetime.now()
        st.write("""### :calendar: ("""+DateFormat(nowTime)+""")""")

        ### getting attribute
        pageDict=st.session_state[self.name]

        ### announcement strip
        announcement_text=None
        if announcement_text:
            st.write("## :trumpet: Anouncement :trumpet:")
            annotated_text( (announcement_text,"","#8ef") )

        ### session quote
        if st.checkbox("Quote of the Session"):
            st.write("### 🤔 Quote of the session:")
            if "qofts" not in pageDict.keys() or st.button("Get Quote"):
                pageDict['qofts']=infra.GetQuote()
            infra.ShowInfo(pageDict['qofts'])

        st.write("---")

        # check if token already exists
        try:
            if pageDict['token']:
                st.write(":white_check_mark: Got Token")
            else:
                st.write("No user found")
        except KeyError:
            if st.session_state.debug:
                st.write("No user state set form commandline")

        ### business part       
        st.write("## Access Information")
        st.write("Enter _two_ user access codes to generate token. If you don't have codes please see [here](https://indico.cern.ch/event/1350108/sessions/520750/attachments/2765634/4817284/PDB_access.pdf)")
        ### token options
        st.write("__Token options__")
        # toggle eos use 
        check_eos=st.checkbox("Use EOS for uploading files?", value=True)

        ### cookie manager
        cookie_manager = get_manager()
        cookies = cookie_manager.get_all()
        if st.checkbox("Check cookies?"):
            st.subheader("All Cookies:")
            st.write(cookies)

            if st.checkbox("Update/add Cookie?"):
                cookie = st.text_input("Cookie", key="01")
                val = st.text_input("Value")
                if st.button("Add"):
                    cookie_manager.set(cookie, val) # Expires in a day by default
                    st.write(f"updated {cookie}: {cookie_manager.get(cookie=cookie)}")
        
            if st.checkbox("Delete Cookie?"):
                cookie = st.text_input("Cookie", key="02")
                if st.button("Delete"):
                    cookie_manager.delete(cookie)
            
            st.write("__NB__ Changes will take effect after refresh")
            st.stop()

        ### check for existing access information
        foundACs=False
        # try getting credentials from commandline
        if st.session_state.debug: st.write("sys args:",sys.argv)
        if len(sys.argv)>1:
            st.info("Reading codes from commandLine")
            ac1,ac2=GetCodes(sys.argv)
            #st.write(ac1,",",ac2)
            if ac1==None or ac2==None:
                st.error("Problem recognising credentials. Please check arguments and try again.")
            else:
                # pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                pageDict['ac1'],pageDict['ac2']=ac1,ac2
                foundACs=True
                st.write("- Got codes from commandLine")

                # automatic registration when retrieve credentials from file
                pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)
                # update cookies
                for ac in ["ac1","ac2"]:
                    cookie_manager.set(ac, pageDict[ac], key="11"+ac) # Expires in a day by default

        ### try credentials from cookies
        elif "ac1" in cookies.keys() and "ac2" in  cookies.keys():
            st.info("Reading codes from cookies")
            ac1,ac2=cookies['ac1'],cookies['ac2']
            #st.write(ac1,",",ac2)
            if ac1==None or ac2==None:
                st.error("Problem recognising credentials. Please check arguments and try again.")
            else:
                # pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                pageDict['ac1'],pageDict['ac2']=ac1,ac2
                foundACs=True
                st.write("- Got codes from cookies")

                # automatic registration when retrieve credentials from file
                pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)
                # # update cookies
                # cookie_manager.set('ac1', pageDict['ac1']) # Expires in a day by default
                # cookie_manager.set('ac2', pageDict['ac2']) # Expires in a day by default

                # for ac in ["ac1","ac2"]:
                #     cookie_manager.delete(ac)
                #     cookie_manager.set(ac, pageDict[ac]) # Expires in a day by default
        
        ### getting credentials from user inputs
        if foundACs==False or st.checkbox("Enter codes manually?"):
            # option to drop in file with ACs or token
            infra.ToggleButton(pageDict,'tog_drop',"Enter credentials via file?")
            if pageDict['tog_drop']:
                st.write('Please upload a _json_ file with format:')
                st.json({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"})
                ## drag and drop method
                pageDict['file'] = st.file_uploader('Upload JSON file', type="json")
                if st.session_state.debug:
                    st.write("uploaded file:",pageDict['file'])

                if pageDict['file'] is not None:
                    credDict = json.load(pageDict['file'])

                    # format keys (use same dictionary)
                    for ac in ["AC1","AC2"]:
                        if ac.lower() in [k.lower() for k in credDict.keys()]:
                            try:
                                pageDict[ac.lower()]=credDict[ac.lower()]
                            except KeyError:
                                pageDict[ac.lower()]=credDict[ac]
                            except:
                                st.write(f"Cannot find {ac} in file")

                    # automatic registration when good credentials uploaded
                    pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                    st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)
                    # update cookies
                    for ac in ["ac1","ac2"]:
                        cookie_manager.set(ac, pageDict[ac], key="12"+ac) # Expires in a day by default

                else:
                    st.write("No file uploaded")
                    st.download_button(label="Download example JSON", data=json.dumps({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"}, indent=2), file_name="credentials.json")

            ### input passwords by hand
            else:
                infra.TextBox(pageDict, 'ac1', 'Enter password 1', True)
                infra.TextBox(pageDict, 'ac2', 'Enter password 2', True)

                # button to get token
                if st.button("Get PDB Token"):
                    pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                    st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)
                    # update cookies
                    for ac in ["ac1","ac2"]:
                        cookie_manager.set(ac, pageDict[ac], key="13"+ac) # Expires in a day by default

        # ### check credentials set
        # if "ac1" in pageDict.keys() and "ac2" in pageDict.keys():

        #     if st.session_state.debug:
        #         st.write("**DEBUG** tokens")
        #         st.write("ac1:",pageDict['ac1'],", ac2:",pageDict['ac2'])

        #     # automatic registration when credentials uploaded
        #     pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
        #     st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)


        #     if pageDict['ac1']!=None and pageDict['ac1']!=None:
        #         # button to (re)get token
        #         if st.button("Get PDB Token"):
        #             pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
        #             st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)

        #     else:
        #         st.write("Token missing. Check inputs")
        
        # else:
        #     st.info("Please enter credentials")
        #     st.stop()
            
        try:
            if st.session_state.myClient==None:
                st.error("### Authentication has failed.")
                st.write("Please check user credentials.")
                st.stop()

            else:
                # button to (re)get token
                if st.button("Get new PDB Token"):
                    pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                    st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)
                    # update cookies
                    for ac in ["ac1","ac2"]:
                        cookie_manager.set(ac, pageDict[ac], key="14"+ac) # Expires in a day by default
        
        except AttributeError:
            st.write("Please enter credentials")

        try:
            st.write("Registed at",pageDict['time'])
            exTime = datetime.datetime.fromtimestamp(st.session_state.myClient.user.expires_at)
            st.write("Token expires at: "+exTime.strftime("%d.%m.%Y %H:%M"))
                        
            if st.session_state.debug:
                st.write(f"**DEBUG** re-register (eos=={str(check_eos)})")
                if st.button("re-get Token"):
                    pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                    st.session_state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'],check_eos)
                    # update cookies
                    for ac in ["ac1","ac2"]:
                        cookie_manager.set(ac, pageDict[ac], key="15"+ac) # Expires in a day by default

            # feedback on passwords
            try:
                pageDict['user']=st.session_state.myClient.get('getUser', json={'userIdentity': st.session_state.myClient.user.identity})
                st.info("Returning token for "+str(pageDict['user']['firstName'])+" "+str(pageDict['user']['lastName'])+" seems ok. (check debug for details)")
                if st.session_state.debug:
                    st.write("User (id):",pageDict['user']['firstName'],pageDict['user']['lastName'],"("+pageDict['user']['id']+")")
            except:
                st.error("Bad token registered. Please close streamlit and try again")
        except KeyError:
            st.info("No token yet registed")


        if "myClient" in list(st.session_state.keys()):
            resetLists=False
            ### reset option
            st.write("### Collecting data")
            prog_text = st.empty()
            if st.button("reset lists"):
                resetLists=True
            ### gather lists
            if "instList" not in list(pageDict.keys()) or resetLists:
                prog_text.text("** Please wait for *Institutions* to be compiled **")
                st.write("Get *Institutions*")
                pageDict['instList']=DBaccess.GetInstList(st.session_state.myClient)
                pageDict['instList'] = sorted(pageDict['instList'], key=lambda k: k['name'])
                st.write("Done!")
                if "inst" not in pageDict.keys():
                    pageDict['inst']=[x for x in pageDict['instList'] if x['code']==pageDict['user']['institutions'][0]['code']][0]
            else:
                st.write("Got institutions")

            if "projList" not in list(pageDict.keys()) or resetLists:
                prog_text.text("** Please wait for *Projects* to be compiled **")
                st.write("Get *Projects*")
                pageDict['projList']=DBaccess.GetProjList(st.session_state.myClient)
                pageDict['projList'] = sorted(pageDict['projList'], key=lambda k: k['name'])
                st.write("Done!")
                if "proj" not in pageDict.keys():
                    if pageDict['user']['preferences']['defaultProject']!=None:
                        pageDict['proj']=[x for x in pageDict['projList'] if x['code']==pageDict['user']['preferences']['defaultProject']][0]
                    else:
                        st.write("__No defaultProject found__")
                        pageDict['proj']=None

            else:
                st.write("Got projects")
            prog_text.text("** Lists are compiled **")
            st.success("Ready for Apps!")

            st.write("### Current user settings")
            if len(st.session_state['Authenticate']['user']['institutions'])>1:
                st.write("Multiple user institutes found")
                infra.ToggleButton(pageDict,'togInst',"select other?")
                if pageDict['togInst']:
                    infra.SelectBox(pageDict,'selInst',st.session_state['Authenticate']['user']['institutions'],'Select user Institution','name')
                    pageDict['inst']=[x for x in pageDict['instList'] if x['code']==pageDict['selInst']['code']][0]

            st.write("Institute:",pageDict['inst']['name'])
            # select project option
            if st.checkbox('Select other project') or pageDict['proj']==None:
                pageDict['proj']=st.radio("Select project:",pageDict['projList'], format_func=lambda x: x['name'])
                st.stop()
            st.write("Project:",pageDict['proj']['name'])
        

            ### calendars
            st.write("---")

            projMap=[  {'code':"P",'name':"pixels",'calID':"YzY4YjhmODdiZTVkMjg2Mzc2MzViODk3NGFkYzA4NzU4M2ZiMTc3MWQzOGJiYjI4MTI1NGIzOGYyOGQxM2EzMkBncm91cC5jYWxlbmRhci5nb29nbGUuY29t", 'indico':"https://indico.cern.ch/category/5332/"},
                        {'code':"S",'name':"strips",'calID':"MGYxYjMzOGEyN2M0ZDY1NzYyMWI5ZGM2YWU2YjMzNGI0MDYwM2Y0NzM2ZjE3NGQzN2EzZTYxOTY3NmI0NTFlYUBncm91cC5jYWxlbmRhci5nb29nbGUuY29t", 'indico':"https://indico.cern.ch/category/5333/"},
                        {'code':"G",'name':"general",'calID':"YjQyYzQzNDA3ZDkwYzY2MzA0MGU0MzZhMTEzZDQ2N2RjZDg0NjZmMGNmOGI5N2ZhMzgzYzY2MjdhOThkYzliOEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t", 'indico':"https://indico.cern.ch/category/5331/"} ]
            
            pageDict['calSel']=next((item['name'] for item in projMap if item['code']==pageDict['proj']['code']), None)

            if st.checkbox("Select other calendar?"):
                infra.SelectBox(pageDict,'calSel',[pm['name'] for pm in projMap],"Select calender:")
            
            if pageDict['calSel']==None:
                st.stop()
            
            st.write(f"### {pageDict['calSel'].title()} Calendar")
            ### get calender dictionary
            calObj=next((item for item in projMap if item['name']==pageDict['calSel']), None)

            st.write(f"Link to indico [schedule]({calObj['indico']})")
            components.iframe(
            f"https://calendar.google.com/calendar/embed?src={calObj['calID']}&ctz=Europe%2FZurich",
            width=1200, height=800, scrolling=True)
            
        else:
            st.write("Get info when registered")

        
