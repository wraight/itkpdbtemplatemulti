from core.MultiApp import App

smalls={
        'built on': 'built on: BUILDDATE',
        'git repo.':"https://gitlab.cern.ch/wraight/itkpdbtemplatemulti",
        'current commit': ' - built with: COMMITCODE',
        # 'docker repo.':"https://hub.docker.com/repository/docker/kwraight/pointer-app",
        'contact': "📩 wraightATcernDOTch"}

myapp = App("Pointer App", "Pointers to webApps & reports", smalls)

myapp.main()
