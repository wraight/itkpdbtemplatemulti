from core.MultiApp import App

smalls={
    'git':"https://gitlab.cern.ch/wraight/itkpdbtemplatemulti",
    'current commit': ' - built with: COMMITCODE',
    # 'docker':"https://hub.docker.com/repository/docker/kwraight/itk-multi-app",
    'other':"otherstuff"
}

myapp = App("itkTempMulti", "streamlit ITk PDB Multi Theme App", smalls)

myapp.main()
