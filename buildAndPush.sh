docker build . -f dockerFiles/Dockerfile -t kwraight/itk-pdb-template-multi:1.0
docker push kwraight/itk-pdb-template-multi:1.0
docker build . -f dockerFiles/Dockerfile -t kwraight/itk-pdb-template-multi:latest
docker push kwraight/itk-pdb-template-multi:latest
docker build . -f dockerFiles/DockerfileCern -t kwraight/itk-pdb-template-multi:cern
docker push kwraight/itk-pdb-template-multi:cern
