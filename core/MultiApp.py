import streamlit as st
import corePages
import userPages
###
import os
import sys
from datetime import datetime
import core.DBaccess as DBaccess
import core.stInfrastructure as infra

#####################
### useful functions
#####################

def toggle_debug():
    st.session_state.debug = not st.session_state.debug


class App:

    def __init__(self, name, title, smalls):
        self.name = name
        self.title = title
        self.smalls = smalls
        self.state = {}

        self.init_themes()

    def init_themes(self):
        self.themes = userPages.__all__.keys()
        return

    def init_pages(self,theme=None):
        try:
            self.pages.clear()
        except AttributeError:
            self.pages = dict()
        allPages = []
        allPages = corePages.__all__.copy()
        if theme!=None:
            allPages += userPages.__all__[theme].copy()
        # order pages if required
        #allPages.insert(0, allPages.pop([p().name for p in allPages.index("NAME")))
        allPages.append(allPages.pop([p().name for p in allPages].index("Broom Cupboard")))
        for page in allPages:
            p = page() #self.state)
            self.pages[p.name] = p
        #return {theme:[len(allPages),len(corePages.__all__),len(userPages.__all__),len(self.pages.keys())]}
        #return {theme:[p().name for p in allPages]} #self.pages.keys()}
        return [{k:[p().name for p in v]} for k,v in userPages.__all__.items()]

#####################
### main part
#####################

    def main(self):
        st.sidebar.title(self.title)

        ### sidebar
        st.sidebar.markdown("### :zap: Powered by [itkdb](https://pypi.org/project/itkdb/) :zap:")
        st.sidebar.markdown("---")

        ### theme and page selection
        theme = st.sidebar.radio("Select app by theme: ", tuple(self.themes))
        self.init_pages(theme)
        name = st.sidebar.radio("Select page: ", tuple(self.pages.keys()))

        ### check session state attribute, set if none
        try:
            if name in st.session_state[theme].keys():
                if st.session_state.debug: st.sidebar.markdown("session_state \'"+theme+"."+name+"\' OK")
            else:
                st.session_state[theme][name]={}
                if st.session_state.debug: st.sidebar.markdown("session_state \'"+theme+"."+name+"\' defined")
        except KeyError:
            st.session_state[theme]={name:{}}

        ### renew token button (default hidden)
        try:
            if st.session_state.debug:
                st.sidebar.markdown("---")
                try:
                    if st.session_state.myClient:
                        exTime = datetime.fromtimestamp(st.session_state.myClient.user.expires_at)
                        st.sidebar.markdown("Token expires at: "+exTime.strftime("%d.%m.%Y %H:%M"))
                    if st.session_state.Authenticate['ac1'] and st.session_state.Authenticate['ac2']:
                        if st.sidebar.button("Renew Token"):
                            nowTime = datetime.now()
                            st.session_state.Authenticate['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                            st.sidebar.markdown("Registed at: "+st.session_state.Authenticate['time'])
                            st.session_state.myClient=getattr(DBaccess,"AuthenticateUser")(st.session_state.Authenticate['ac1'],st.session_state.Authenticate['ac2'])
                    else:
                        st.sidebar.markdown("register on top page")
                except AttributeError:
                    st.sidebar.markdown("No client set")
        except AttributeError:
            pass

        st.sidebar.markdown("---")

        ### inst, proj data (user's by default)
        if "Authenticate" in [x for x in st.session_state.keys()]:
            try:
                st.session_state.Authenticate['inst']=st.sidebar.selectbox("Institution:", st.session_state.Authenticate['instList'], format_func=lambda x: x['code'], index=st.session_state.Authenticate['instList'].index(st.session_state.Authenticate['inst']))
            except KeyError:
                st.sidebar.markdown("No instList set")
            try:
                st.session_state.Authenticate['proj']=st.sidebar.selectbox("Project:", st.session_state.Authenticate['projList'], format_func=lambda x: x['code'], index=st.session_state.Authenticate['projList'].index(st.session_state.Authenticate['proj']))
            except KeyError:
                st.sidebar.markdown("No projList set")
        else:
            st.sidebar.markdown("lists not found")

        st.sidebar.markdown("---")

        ### mini-state summary
        showKeys = st.sidebar.checkbox("Show page history")
        if showKeys:
            mykeys=[x for x in st.session_state.keys()]
            # st.sidebar.markdown(myatts)
            for mk in mykeys:
                if st.sidebar.checkbox("**"+str(mk)+"** defined"):
                    if mk not in ["broom","myClient","Authenticate","debug"]: # no deletes
                        if st.sidebar.button(f"delete {mk} info?"):
                            del st.session_state[mk]
                    else:
                        st.sidebar.markdown("No delete permitted")
            # if st.sidebar.button("Clear Selections"):
            #     for mk in mykeys:
            #         if mk=="broom" or mk=="myClient" or mk=="Authenticate" or mk=="debug": continue
            #         del st.session_state[mk]

        st.sidebar.markdown("---")

        ### debug toggle
        try:
            debug = st.sidebar.checkbox("Toggle debug",value=st.session_state.debug)
        except AttributeError:
            debug = st.sidebar.checkbox("Toggle debug")
        if debug:
            st.session_state.debug=True
        else: st.session_state.debug=False

        ### small print
        st.sidebar.markdown("---")
        st.sidebar.markdown("**Small Print**")
        st.sidebar.markdown("_Code Heirarchy_")
        st.sidebar.markdown(f"streamlitTemplate: \n - {infra.Version()['date']} ({infra.Version()['sha']})")
        st.sidebar.markdown(f"itkPdbTemplate: \n - {DBaccess.Version()['date']} ({DBaccess.Version()['sha']})")
        st.sidebar.markdown("_Additional Information_")
        for k,v in self.smalls.items():
            if "http" in v: # links
                st.sidebar.markdown("["+k+"]("+v+")")
            else:
                st.sidebar.markdown(v)

        ### get page
        self.pages[name].main()
