import streamlit as st
###
import itkdb
import itkdb.exceptions as itkX

#####################
### Things
#####################

def Version():
    return {'date':"VERSION_DATE",'sha':"VERSION_SHA"}

def AuthenticateUser(ac1,ac2, useEos=False):
    user = itkdb.core.User(access_code1=ac1, access_code2=ac2)
    try:
        user.authenticate()
        client = itkdb.Client(user=user, use_eos=useEos)
        return client
    except itkX.ResponseException:
        st.error("Cannot authenticate user credentials")
        return None

@st.cache(suppress_st_warning=True)
def DbGet(client, myAction, inData, listFlag=False):
    outData=None
    if listFlag:
        try:
            outData = list(client.get(myAction, json=inData ) )
        except itkX.BadRequest as b: # catch double registrations
            st.write(myAction+": went wrong for "+str(inData))
            st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    else:
        try:
            outData = client.get(myAction, json=inData)
        except itkX.BadRequest as b: # catch double registrations
            st.write(myAction+": went wrong for "+str(inData))
            st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return outData

@st.cache(suppress_st_warning=True)
def DbPost(client, myAction, inData):
    outData=None
    try:
        outData=client.post(myAction, json=inData)
    except itkX.BadRequest as b: # catch double registrations
        st.write(myAction+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
        try:
            st.write('**'+str(b)[str(b).find('"paramMap": ')+len('"paramMap": '):-8]+'**') # sucks
        except:
            pass
    except itkX.ServerError as b: # catch double registrations
        st.write(myAction+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return outData

# @st.cache
# def GetInstList(client):
@st.cache_data
def GetInstList(_client):
    myList=[]
    try:
        myList = list(_client.get('listInstitutions'))
    except itkX.BadRequest as b: # catch double registrations
        st.write('listInstitutions'+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return myList

# @st.cache
# def GetProjList(client):
@st.cache_data
def GetProjList(_client):
    myList=[]
    try:
        myList= list(_client.get('listProjects'))
    except itkX.BadRequest as b: # catch double registrations
        st.write('listProjects'+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return myList
